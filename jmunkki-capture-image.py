# from https://github.com/gphoto/libgphoto2/issues/30#issuecomment-346974478
import gphoto2 as gp
import logging
import time

def setup_logging():
    logging.basicConfig()
    gp.check_result(gp.use_python_logging(mapping={
        gp.GP_LOG_ERROR   : logging.INFO,
        gp.GP_LOG_DEBUG   : logging.DEBUG,
        gp.GP_LOG_VERBOSE : logging.DEBUG - 3,
        gp.GP_LOG_DATA    : logging.DEBUG - 6}))
    logging.getLogger('gphoto2').setLevel(logging.INFO)

setup_logging()
context = gp.gp_context_new()
camera = gp.check_result(gp.gp_camera_new())
gp.check_result(gp.gp_camera_init(camera, context))

def cameraAction(action, value):
    config = camera.get_config(context)
    target = config.get_child_by_name(action)
    target.set_value(value)
    camera.set_config(config, context)

# based on: https://www.cmcguinness.com/2015/11/using-python-and-gphoto2-to-control-the-focus-of-a-canon-t3i-eos-600d/
def doFocusStep(change, dt=0.2):
    time.sleep(dt)
    cameraAction('manualfocusdrive', change)
    time.sleep(dt)

def fAt(focus=None):
    if focus is not None:
        cameraAction('viewfinder', 1)
        for i in range(1,10):
            doFocusStep('Near 3')
        big = int(focus)
        small = int((focus - big) * 10)
        tiny = int(100 * focus - big * 100 - small * 10)
        for i in range(0,big):
            doFocusStep('Far 3')
        for i in range(0,small):
            doFocusStep('Far 2')
        for i in range(0,tiny):
            doFocusStep('Far 1', dt=0.1)
        cameraAction('viewfinder', 0)

def capAt(focus=None):
    fAt(focus)
    cameraAction('eosremoterelease', 'Press Full')
    cameraAction('eosremoterelease', 'Release Full')
    done = False
    while not done:
        n,file_path = camera.wait_for_event(1000, context)
        if n == 2:
            print(file_path.folder, file_path.name)
            camera_file = camera.file_get(file_path.folder, file_path.name, gp.GP_FILE_TYPE_NORMAL, context)
            gp.check_result(gp.gp_file_save(camera_file, file_path.name))
        elif n == 1:
            done = True
        elif n != 0:
            print(n,file_path)

cameraAction('viewfinder', 1)
cameraAction('viewfinder', 0)
capAt()
capAt(3.5)
