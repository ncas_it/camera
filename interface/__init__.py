#!/usr/bin/env python3
import subprocess
import os
import select
import csv
import urllib
import exifread as ef
import socket


from pystemd.systemd1 import Unit
from pystemd.dbuslib import DBus

from pathlib import Path
from datetime import datetime
from time import time

from flask import Flask, request, render_template, send_file, redirect, url_for, make_response, send_from_directory

from systemd import journal
from subprocess import run, Popen, DEVNULL

from functools import update_wrapper

configfilepath = Path(Path(__file__).parent.parent.absolute(),'intervalrc')
print('Loading config from {}'.format(configfilepath,))

# borrowed from 
# https://gist.github.com/snakeye/fdc372dbf11370fe29eb 
def _convert_to_degress(value):
    """
    Helper function to convert the GPS coordinates stored in the EXIF to degress in float format
    :param value:
    :type value: exifread.utils.Ratio
    :rtype: float
    """
    d = float(value.values[0].num) / float(value.values[0].den)
    m = float(value.values[1].num) / float(value.values[1].den)
    s = float(value.values[2].num) / float(value.values[2].den)

    return d + (m / 60.0) + (s / 3600.0)

def get_ip():
    """
    returns the IP address of the default route (usually wlan0)
    """
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.settimeout(0)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except Exception:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP

def get_cpu_temp():
    """
    Return the CPU temperature
    """
    with open('/sys/class/thermal/thermal_zone0/temp','r') as tf:
        return '{:.1f}'.format(int(tf.read())/1000.0,)

def nocache(f):
    def new_func(*args, **kwargs):
        resp = make_response(f(*args, **kwargs))
        resp.headers['Cache-Control'] = 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0'
        return resp
    return update_wrapper(new_func, f)

def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)

    if test_config is None:
        # load the instance config, if it exists, when not testing
        with open(configfilepath, 'r') as configfile:
            reader = csv.reader((row for row in configfile if not row.startswith('#')), delimiter='=', escapechar='\\', quoting=csv.QUOTE_MINIMAL)
            config = {}
            for key, value in reader:
                config[key] = value
            app.config.from_mapping(config, silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass   

    def format_log(entry):
        try:
            return '{} {}\n'.format(
                            entry['__REALTIME_TIMESTAMP'].strftime('%Y-%m-%d %H:%M:%S'),
                            entry['MESSAGE'],
            )
        except KeyError:
            print(entry)
            return ''

    @app.route('/favicon.ico')
    def favicon():
        return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')

    @app.route('/log')
    @nocache
    def get_log():
        '''Connect to the SystemD user-level interval service log and display the contents'''
        log = journal.Reader()
        log.this_boot()
        log.log_level(journal.LOG_INFO)
        log.add_match(_SYSTEMD_USER_UNIT="interval.service")
    
        #move to (near)end of journal for that unit
        log.seek_realtime(time())
        #discard old entries except last 15
        log.get_previous(skip=15)

        p = select.poll()

        #Register the journals's file descriptor with the polling object
        log_fd = log.fileno()
        poll_ev_mask = log.get_events()
        p.register(log_fd, poll_ev_mask)

        def generate():
            #send back most recent 15 entries
            for entry in log:
                yield format_log(entry)

            while p.poll():
                if log.process() != journal.APPEND:
                    continue
                entry = log.get_next()
                yield format_log(entry)
    
        return app.response_class(generate(), mimetype='text/plain')

    @app.route('/latest.jpg')
    @nocache
    def latest():
        '''get latest image'''
        outpath = Path(app.config['DIR'],datetime.now().strftime('%Y/%m/%d'))
        latest_file = max(outpath.glob('*.jpg'), key=lambda p: p.stat().st_ctime)
        return send_file(open(latest_file, 'rb'), mimetype='image/jpeg')

    @app.route('/latest-secondary.jpg')
    @nocache
    def latest_secondary():
        '''get latest image from secondary camera'''
        second_file = urllib.request.urlopen('http://' + app.config['SECONDARY'] + ':5000/latest.jpg')
        return send_file(second_file, mimetype='image/jpeg')

    @app.route('/thumb.jpg')
    @nocache
    def thumb():
        '''gets EXIF thumbnail from latest image'''
        outpath = Path(app.config['DIR'],datetime.now().strftime('%Y/%m/%d'))
        latest_file = max(outpath.glob('*.jpg'), key=lambda p: p.stat().st_ctime)

        with open(latest_file, 'rb') as f:
            tags = ef.process_file(f)
            thumb = tags.get('JPEGThumbnail')
            if thumb:
                response = make_response(thumb)
                response.headers.set('Content-Type', 'image/jpeg')
                return response
            
    @app.route('/thumb-secondary.jpg')
    @nocache
    def thumb_secondary():
        '''get thumb image from secondary camera'''
        second_file = urllib.request.urlopen('http://' + app.config['SECONDARY'] + ':5000/thumb.jpg')
        return send_file(second_file, mimetype='image/jpeg')

    @app.route('/gps')
    @nocache
    def gps():
        '''Return GPS coordinates, if possible'''
        outpath = Path(app.config['DIR'],datetime.now().strftime('%Y/%m/%d'))
        latest_file = max(outpath.glob('*.jpg'), key=lambda p: p.stat().st_ctime)

        with open(latest_file, 'rb') as f:
            tags = ef.process_file(f)
            latitude = tags.get('GPS GPSLatitude')
            latitude_ref = tags.get('GPS GPSLatitudeRef')
            longitude = tags.get('GPS GPSLongitude')
            longitude_ref = tags.get('GPS GPSLongitudeRef')
            if latitude:
                lat_value = _convert_to_degress(latitude)
                if latitude_ref.values != 'N':
                    lat_value = -lat_value
            else:
                return "GPS Not Available"
            if longitude:
                lon_value = _convert_to_degress(longitude)
                if longitude_ref.values != 'E':
                    lon_value = -lon_value
            else:
                return "GPS Not Available"
            return 'Latitude: {:.5f}, Longitude: {:.5f}'.format(lat_value, lon_value)
        return "GPS Not Available" 
        
    @app.route('/gps-secondary')
    @nocache
    def gps_secondary():
        '''get latest gps from secondary camera'''
        second_file = urllib.request.urlopen('http://' + app.config['SECONDARY'] + ':5000/gps')
        return second_file.read()


    @app.route('/')
    def dashboard():
        ''' Main control dashboard '''
        with DBus(user_mode=True) as dbus:
            dbus.open()
            unit = Unit(b'interval.service', bus=dbus, _autoload=True)

            return render_template('dashboard.html', 
                unit=unit.Unit,
                config=app.config,
                myip=get_ip(),
                cpu_temp=get_cpu_temp(),
            )

    @app.route('/ping')
    def ping_secondary():
        pingout = run(['ping', '-c1', app.config['SECONDARY']], text=True, capture_output=True)
        return render_template('ping.html', 
            pingout=pingout.stdout,
        )
        
    @app.route('/alive')
    def alive():
        return "Yes"

    @app.route('/control', methods=('POST',))
    def control():
        if 'go' in request.form:
            app.config['INTERVAL'] = request.form['interval']
            app.config['FRAMES'] = request.form['frames']
            with open(configfilepath, 'w') as configfile:
                writer = csv.writer(configfile, dialect='unix', delimiter='=', escapechar='\\', quoting=csv.QUOTE_MINIMAL)
                for each in ['FRAMES', 'INTERVAL', 'DIR', 'SECONDARY', 'SECONDARY_DIR']:
                    writer.writerow([each, app.config[each]])

            run(['systemctl', '--user', 'start', 'interval.service'])
        elif 'stop' in request.form:
            run(['systemctl', '--user', 'stop', 'interval.service'])
            
        return redirect(url_for('dashboard'))    
    
    @app.route('/reboot')
    def reboot():
        run(['sudo', 'systemctl', 'reboot'])

        return "Restarting"

    @app.route('/reboot-secondary')
    @nocache
    def reboot_secondary():
        '''Reboot secondary camera'''
        second_file = urllib.request.urlopen('http://' + app.config['SECONDARY'] + ':5000/reboot')
        return second_file.read()

    @app.route('/cputemp')
    @nocache
    def cputemp():
        return get_cpu_temp() + '°C'
        
    @app.route('/cputemp-secondary')
    @nocache
    def cputemp_secondary():
        second_file = urllib.request.urlopen('http://' + app.config['SECONDARY'] + ':5000/cputemp')
        return second_file.read()
        
    return app
