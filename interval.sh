#!/bin/sh

# takes one photo every n seconds up to a maximum of m

FOLDER=$(date +%Y/%m/%d)

mkdir -p ${FOLDER}

INTERVAL=${1:-1}
FRAMES=${2:-0}

gphoto2 --capture-image-and-download --filename /tmp/wibble.jpg

gphoto2 --get-config /main/capturesettings/shutterspeed
gphoto2 --get-config /main/capturesettings/aperture

#gphoto2 --capture-preview --capture-image-and-download --interval ${INTERVAL} --frames ${FRAMES} --filename ${FOLDER}/$(date +%Y)-%m-%d-%H%M%S.%C
