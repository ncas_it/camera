#!/usr/bin/env python3
# vim: set et ts=4

from __future__ import print_function

import logging
import os
import pathlib
from datetime import datetime
import time
import subprocess
import sys

import threading

import gphoto2 as gp

class RepeatedTimer(object):
    def __init__(self, interval, function, *args, **kwargs):
    	self._timer = None
    	self.interval = interval
    	self.function = function
    	self.args = args
    	self.kwargs = kwargs
    	self.is_running = False
    	self.next_call = time.time()
    	self.start()

    def _run(self):
        self.is_running = False
        self.start()
        self.function(*self.args, **self.kwargs)

    def start(self):
        if not self.is_running:
            self.next_call += self.interval
            self._timer = threading.Timer(self.next_call - time.time(), self._run)
            self._timer.start()
            self.is_running = True

    def stop(self):
        self._timer.cancel()
        self.is_running = False


event_texts = {
    gp.GP_EVENT_CAPTURE_COMPLETE: "Capture Complete",
    gp.GP_EVENT_FILE_ADDED: "File Added",
    gp.GP_EVENT_FOLDER_ADDED: "Folder Added",
    gp.GP_EVENT_TIMEOUT: "Timeout",
}

serial_numbers = {
    '583053001031': "NCAS-EOS-A",
    '583053001033': "NCAS-EOS-B",
}

frames = 0

def event_text(event_type):
    if event_type in event_texts:
        return event_texts[event_type]
    return "Unknown Event"

def get_config_choices(config_item):
    targets = config_item.count_choices()
    target = 0
    while target < targets:
        print(target, config_item.get_choice(target))
        target = target + 1

def get_cameras():
    camera_objects = []

    port_info_list = gp.PortInfoList()
    port_info_list.load()
    if hasattr(gp, 'gp_camera_autodetect'):
        # gphoto2 version 2.5+
        cameras = gp.check_result(gp.gp_camera_autodetect())
    else:
        port_info_list = gp.PortInfoList()
        port_info_list.load()
        abilities_list = gp.CameraAbilitiesList()
        abilities_list.load()
        cameras = abilities_list.detect(port_info_list)
    for name, addr in cameras:
        camera = gp.Camera()
        idx = port_info_list.lookup_path(addr)
        camera.set_port_info(port_info_list[idx])
        camera.init()
        cfg = camera.get_config()
        child_cfg = cfg.get_child_by_name('ownername')
        if len(child_cfg.get_value()) == 0:
            #nickname is unset
            child_cfg.set_value(serial_numbers[camera.get_config().get_child_by_name('eosserialnumber').get_value()])
        else:
            print("ownername", child_cfg.get_value())

        #exposuremode = cfg.get_child_by_name('autoexposuremode')
        #get_config_choices(exposuremode)

        capturetarget_cfg = cfg.get_child_by_name('capturetarget')

        capturetarget = capturetarget_cfg.get_value()
        capturetarget_cfg.set_value('Memory card')#

        camera.set_config(cfg)

        camera_objects.append(camera)
    return camera_objects

def save_image(camera, data):
    timestamp = datetime.now()
    #print(camera.set_value('capturetarget',0))
    print('Camera file path: {0}/{1}'.format(data.folder, data.name))
    f1 = camera.file_get(data.folder, data.name, gp.GP_FILE_TYPE_NORMAL)

    #make output directory
    targetdir = pathlib.Path(timestamp.strftime('%Y/%m/%d/%H'))
    #equivalent in Python ≥ 3.5 to bash's 'mkdir -p'
    targetdir.mkdir(parents=True, exist_ok=True)

    target = targetdir.joinpath(timestamp.strftime('%Y-%m-%dT%H%M%S.%f-' + camera.get_config().get_child_by_name('ownername').get_value() + pathlib.PurePath(data.name).suffix))
    print('Copying image {0} to {1}'.format(data.name, target))
    f1.save(target.as_posix())
    camera.file_delete(data.folder, data.name)

def set_exposure(camera, exposure = {}):
    if exposure: # empty dictionary is falsy
        cfg = camera.get_config()
        print(cfg.get_child_by_name('drivemode').get_value())
        cfg.get_child_by_name('autoexposuremode').set_value('Manual')
        for each in ['iso', 'aperture', 'shutterspeed']:
            cfg.get_child_by_name(each).set_value(exposure[each])
        camera.set_config(cfg)

def capture(camera, copy=False):
    global frames
    captured = False
    while captured == False:    
        try:
            #data = camera.capture(gp.GP_CAPTURE_IMAGE)
            if gp.gp_camera_trigger_capture(camera) == gp.GP_OK:
                captured = True
        except:
            print("sleeping")
            time.sleep(0.05)
    print('Image taken at {0}'.format(datetime.now().isoformat(),))
    frames = frames + 1

    '''if copy:
        save_image(camera, data)

    #clear the event queue, check for further files
    typ, data = camera.wait_for_event(10000)
    while typ != gp.GP_EVENT_TIMEOUT:

        #print("Event: %s, data: %s" % (event_text(typ),data))

        if typ == gp.GP_EVENT_FILE_ADDED:
            try:
                #save_image(camera, data)
                print("Unexpected file download")
            except gp.GPhoto2Error as err:
                print(err)
        #try to grab next event
        typ, data = camera.wait_for_event(1)'''



def main():
    global frames

    cameras = get_cameras()

    #do a preview shot with the first camera
    preview = cameras[0].capture(gp.GP_CAPTURE_IMAGE)
    cfg = cameras[0].get_config()
    exposure = {}
    for each in ['iso', 'aperture', 'shutterspeed']:
        child_cfg = cfg.get_child_by_name(each)
        exposure[each] = child_cfg.get_value()

    #lock-up mirror
    for camera in cameras:
        camera.capture_preview()
        set_exposure(camera, exposure)

    thr=[]
    count =  5
    interval = 1
    for camera in cameras:
        print("Starting camera")
        thr.append(RepeatedTimer(interval, capture, camera))


    time.sleep(3 + count * interval)
    while frames < count:
        print(frames)
        time.sleep(1)
   
    for each in thr:
        each.stop()
     
    for camera in cameras:
        #un lock-up mirror
        while cfg == False:
            try:
                cfg = camera.get_config()
            except:
                time.sleep(0.05)
        capt = cfg.get_child_by_name('capture')
        capt.set_value(0)
        camera.set_config(cfg)


        #disconnect
        camera.exit()





if __name__ == "__main__":
    sys.exit(main())
