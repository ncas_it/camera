#!/bin/sh
DEVICE=${1:-"wlan1"}

LEASES=$(cat /var/lib/misc/dnsmasq.leases | wc -l)
# iw and rfkill are in the sbin directory
PATH=${PATH}:/usr/sbin

echo "${DEVICE}: ${LEASES} found"

if [ "1" -gt "${LEASES}" ]; then # if anyone is connected to the hotspot

    systemctl stop hostapd@${DEVICE}

    #physical id
    PHYSICAL=phy$(iw dev ${DEVICE} info | grep wiphy | awk '{print $2}')

    RFKILL_ID=$(rfkill -o ID,DEVICE | grep ${PHYSICAL} | awk '{print $1}')

    echo "Trying to block ${RFKILL_ID}"

    rfkill block $RFKILL_ID
fi
