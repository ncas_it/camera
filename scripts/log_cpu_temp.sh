#!/bin/bash
#title          : log_cpu_temp
#description    : This script will log the cpu temperure of the Raspberry Pi and create two files. The first is temperature now and the second file is a log of the temperature over time including a time stamp this will create one new file each day.
#author         : James Groves <james.groves@ncas.ac.uk>
#date           : 20220609
#version        : 0.1
#usage          : ./log_cpu_temp
#notes          : (c) NCAS 2021
#bash_version   : 5.0.3(1)-release (arm-unknown-linux-gnueabihf)
#============================================================================

while :
do
hostname=`hostname`
filename=/data/sysadmin/temp/`date +"%Y-%m-%d"`-"$hostname"-CPU-temp.csv
tempnow=`vcgencmd measure_temp | egrep -o '[0-9]*\.[0-9]*'`
timenow=`date +"%Y-%m-%dT%H:%M:%S%z"`
echo $tempnow > /data/sysadmin/temp/temp.now
echo "$timenow","$tempnow" >> "$filename"
sleep 60
done
