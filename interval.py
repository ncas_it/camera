#!/usr/bin/env python3

# takes one photo every n seconds up to a maximum of m

import argparse

from subprocess import run, Popen, DEVNULL
from datetime import datetime

from pathlib import Path
from socket import gethostname

gphoto_cmd=['gphoto2']

def interval(args):

    
    if args.primary_to:
        ssh_cmd = ['ssh', args.primary_to]
    
    if args.quiet:
        gphoto_cmd.append('--quiet')

    hostname = gethostname()

    try:    
      while True:
    
        folder=datetime.now().strftime('%Y/%m/%d')
        outdir=Path(args.dir,folder)
        
        #make output directory (equiv of mkdir -p)
        outdir.mkdir(parents=True, exist_ok=True)
        
        #unmount camera
        unmount_cmd = ['gio', 'mount', '-u', 'gphoto2://Canon_Inc._Canon_Digital_Camera/', 'gphoto2://Canon_Inc._Canon_Digital_Camera/']
        
        if args.quiet:
            run(unmount_cmd, stdout=DEVNULL, stderr=DEVNULL)
        else:
            run(unmount_cmd)
    
        if not args.secondary:
            #set to auto exposure mode and 1 stop underexposure
            run(gphoto_cmd + [ 
                '--set-config-index', '/main/imgsettings/iso=0', #Auto ISO
                '--set-config-value', '/main/capturesettings/exposurecompensation=-1',
                '--set-config', '/main/capturesettings/autoexposuremode=P',
            ])
    
            #half-press button
            run(gphoto_cmd + [
                '--set-config-index', 'eosremoterelease=1',
            ])
    
            #get exposure details
            output = run(gphoto_cmd + [
                '--get-config=/main/capturesettings/shutterspeed',
                '--get-config=/main/capturesettings/aperture',
                '--get-config=/main/imgsettings/iso'
            ], text=True, capture_output=True)
    
            #release half-press button
            run(gphoto_cmd + [
                '--set-config-index', 'eosremoterelease=3',
            ])
    
            exposure = {}
            for config in output.stdout.split('END'):
                details = config.split('\n') 
                for each in details:
                    if ':' in each:
                        (name, value) = each.split(': ')
                        if name == 'Label':
                            label = value
                        if name == 'Current':
                            current = value
                exposure[label] = current
    
        else:
            #secondary mode, exposure from command line
            exposure = {
                'Shutter Speed': args.shutter,
                'Aperture': args.aperture,
                'ISO Speed': args.iso,
            }
        
    
        if not args.quiet:
            print("Shutter: {Shutter Speed}, Aperture: f/{Aperture}, ISO: {ISO Speed}".format(**exposure))
       
        #Trigger secondary, if requested
        if args.primary_to:
            secondary_process = Popen(ssh_cmd + [
                'python3', '/home/pi/camera/interval.py', 
                '--frames={}'.format(args.frames,),
                '--interval={}'.format(args.interval,),
                '--secondary',
                '--shutter={Shutter Speed}'.format(**exposure), 
                '--aperture={Aperture}'.format(**exposure), 
                '--iso={ISO Speed}'.format(**exposure),
                '--dir', args.secondary_dir])
     
        #set to manual exposure mode
        set_manual = gphoto_cmd + ['--set-config', '/main/capturesettings/autoexposuremode=Manual']
        run(set_manual)
     
        #set exposure 
        set_exposure = gphoto_cmd + [
            '--set-config-value', "/main/capturesettings/shutterspeed={Shutter Speed}".format(**exposure ),
            '--set-config-value', "aperture={Aperture}".format(**exposure),
            '--set-config-value', "iso={ISO Speed}".format(**exposure),
        ]
        run(set_exposure)
    
    
        capture_cmd = gphoto_cmd + [
            '--capture-preview', #to lock up the mirror
            '--capture-image-and-download',
            '--interval', str(args.interval),
            '--frames', str(args.frames),
            '--filename', outdir.joinpath(hostname + '-20%y-%m-%d-%H%M%S-%:')
        ]
        primary_process = Popen(capture_cmd)
    
        #Wait for secondary if requested
        if args.primary_to:
            secondary_process.wait()
        primary_process.wait()
    
        if not args.continuous:
            break;
    except KeyboardInterrupt: #Ctrl-C or SIGINT
        print('Keyboard Interrupt (SIGINT) received')
        reset_cmd = gphoto_cmd + ['--reset']
        if secondary_process:
            print('Terminating secondary process')
            secondary_process.terminate()
            secondary_process.wait()
            print('Resetting secondary gphoto2')
            Popen(ssh_cmd + reset_cmd)
        if primary_process:
            print('Terminating primary process')
            primary_process.terminate()
            primary_process.wait()
        print('Resetting primary gphoto2')
        reset_process = Popen(reset_cmd)
        reset_process.wait()

def resetusb(vendor=0x04a9, product=0x32ca):
    from usb.core import find as finddev
    dev = finddev(idVendor=vendor, idProduct=product)
    dev.reset()
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Takes a timelapse series of photos in batches of f frames separated by i seconds, using the same exposure settings for each frame")
    
    parser.add_argument('--frames', '-f', type=int, default=60, help="Number of exposure-alike frames. Default %(default)s.")
    parser.add_argument('--interval', '-i', type=int, default=1, help="interval in seconds. Default %(default)s.")
    parser.add_argument('--dir', '-d', required=True, help="Output directory")
    parser.add_argument('--quiet', '-q',  action='store_true', help="Reduce output")
    parser.add_argument('--continuous', '-c',  action='store_true', help="Run continuously, re-metering every f frames")
    parser.add_argument('--secondary', '-s',  action='store_true', help="Take exposure details from the command-line instead of camera metering. Intended to allow the primary camera to control the secondary one.")
    parser.add_argument('--primary_to', '-p',  type=str, help="control secondary camera set-up at this address")
    parser.add_argument('--shutter', help="Shutter speed, as a value compatible with the camera (e.g. 1/50, 3). Required if secondary mode.")
    parser.add_argument('--aperture', help="Aperture as an f-number (e.g. 1.8). Required if secondary mode.")
    parser.add_argument('--iso', help="Sensistivity as an ISO-equivalent. (e.g. 400). Required if secondary mode.")
    parser.add_argument('--secondary_dir', help="data dir on secondary set up. Required if secondary mode")
    
    args = parser.parse_args()
    if args.secondary:
        if args.shutter is None or args.iso is None or args.aperture is None or args.secondary_dir:
            parser.error('If in secondary mode, shutter speed, aperture, ISO, and secondary dir must all be set')
    elif args.primary_to:
        if not(args.shutter is None and args.iso is None and args.aperture is None) or args.secondary_dir is None:
            parser.error('If in primary mode mode, shutter speed, aperture, and ISO must not be set and secondary dir must be set')
    else:
        if not(args.shutter is None and args.iso is None and args.aperture is None and args.secondary_dir is None):
            parser.error('If in single-camera mode, shutter speed, aperture, ISO, and secondary dir must not be set')

    interval(args)
